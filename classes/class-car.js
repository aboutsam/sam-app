import { drivers, brands, colorEnum } from './../data/sample'
let _ = require('lodash')

class Car {
  constructor(car) {
    Object.assign(this, car)
  }

  // return random color of colorEnum
  async color() {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(_.sample(colorEnum))
      }, 1000)
    })
  }

  async driver() {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve([_.sample(drivers)])
      }, 1000)
    })
  }

  async brand() {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve([_.sample(brands)])
      }, 1000);
    })
  }
}

export { Car }
