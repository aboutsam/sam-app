import { resolvers } from "./../data/resolvers"
import { typeDefs } from "./../data/schema"
import { ApolloServer, gql } from "apollo-server-express"
import mongoose from "mongoose"
import express from "express"

const portNumber = process.env.GRAPHQL_APP_PORT || 3000
const collectionName = process.env.MONGO_COLLECTION


// first connect with mongodb before we start with server
const startSamServer = async () => {
  const app = express()
  
  const server = new ApolloServer({
    typeDefs,
    resolvers,
  });

  // connection to MongoDB
  await mongoose.connect(`mongodb://localhost:27017/${collectionName}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  server.applyMiddleware({ app })

  app.listen({ port: portNumber }, () =>
    console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
  )
}

startSamServer()