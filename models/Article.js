import { isInteger } from "lodash";
import mongoose from "mongoose"

export const Article = mongoose.model("Article", { id: String, title: String, description: String, url: String });