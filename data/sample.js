import { GraphQLEnumType } from 'graphql'

let cars = [
  { id: "1", enginePower: "75kW", hasTrailerHitch: false },
  { id: "2", enginePower: "58kW", hasTrailerHitch: true },
  { id: "3", enginePower: "122kW", hasTrailerHitch: false },
  { id: "4", enginePower: "300", hasTrailerHitch: true },
  { id: "5", enginePower: "110kW", hasTrailerHitch: true },
  { id: "6", enginePower: "37kW", hasTrailerHitch: false },
];

let drivers = [
  { id: "1", name: "Alice", age: 27 },
  { id: "2", name: "Sam", age: 33 },
  { id: "3", name: "Lana", age: 26 },
  { id: "4", name: "Jördi", age: 30 },
  { id: "5", name: "Tim", age: 21 },
  { id: "6", name: "Cleo", age: 18 },
];

let brands = [
  { id: "1", name: "Audi" },
  { id: "2", name: "BMW" },
  { id: "3", name: "Telsa" },
  { id: "4", name: "Renault" },
]

// example for empty array
let postList = []

let colorEnum = [ 'PURPLE', 'LIGHTBLUE', 'ORANGE' ]

export { cars, drivers, brands, postList, colorEnum }