import { gql } from "apollo-server";
import { ColorEnumType }  from "./../data/sample"
let _ = require('lodash') // tool to wrote fast javascript queries

// schema for GraphQl
const typeDefs = gql`
  type Query {
    getColor: Color
    getBrand(id: ID!): Brand
    getBrands: [Brand]
    getCar(id: ID!): Car
    getCars: [Car]
    getDriver(id: ID!): Driver
    getDrivers: [Driver]
    feed: [Link!]
    getPosts: [Post]
  }

  type Mutation {
    addBrand(name: String): Brand
    addPost(id: Int, title: String, description: String, url: String): Post
    post(url: String!, description: String!): Link!
  }

  type Post {
    id: Int
    title: String
    description: String
    url: String
  }

  type Link {
    id: ID!
    description: String!
    url: String!
  }

  type Car {
    id: Int!
    brand: [Brand]
    color: Color
    enginePower: String
    hasTrailerHitch: Boolean
    driver: [Driver]
  }

  # example of enum like ColorEnumType
  enum Color {
    PURPLE
    LIGHTBLUE
    ORANGE
  }

  type Driver {
    id: Int!
    name: String!
    age: Int
  }

  type Brand {
    id: ID
    name: String
  }
`;

export { typeDefs };
