import { Article } from "../models/Article"
import { Car } from "../classes/class-car"
import { brands, cars, drivers, postList } from "./../data/sample"
let idBrand = brands.length
let idPost = (postList.length === 0) ? 1 : 0; // if array empty start with 1

const resolvers = {
  Query: {
    getColor: () => 'LIGHTBLUE',
    // example to use Class Car for output
    getCars: () => cars.map((car) => new Car(car)),
    getCar(parent, args, context, info) {
      return new Car(cars.find((car) => car.id === args.id));
    },
    getBrands: () => brands,
    getBrand(parent, args, context, info) {
      return brands.find((brand) => brand.id === args.id);
    },
    getDrivers: () => drivers,
    getDriver(parent, args, context, info) {
      return drivers.find((driver) => driver.id === args.id);
    },
    getPosts: () => postList
  },
  Mutation: {
    addBrand: (parent, brand) => {
      brand.id = idBrand++
      brands.push(brand)
      console.log(brands) // return Brand Array in console log
      return brand
    },
    addPost: async (parent, post) => {
      post.id = idPost++
      postList.push(post)
      console.log(postList) // return Post Array in console log

      // const test = { id: 'xxx', name: 'name of what' }
      const article = new Article(post)
      await article.save().then(() => console.log(`Post with ID ${post.id} is saved!`))

      return post
    }
  }
};

export { resolvers };

// const samMongo = new taskStudentJoeDeMongoDb({ name: "Zildjian" })
//   samMongo.save().then(() => console.log("meow"))
