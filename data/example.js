/**
 * Example of Mock Data, if wanna use for Testing
 * 
 * Don't forget to import `casual` and `MockList`
 */

const mocks = {
  Int: () => casual.integer(1, 100),
  Person: () => ({
    name: casual.name,
    age: () => casual.integer(16, 67),
  }),
  Driver: () => ({
    name: casual.first_name,
    age: () => casual.integer(16, 67),
  }),
  Car: () => ({
    name: casual.name,
    enginePower: () => casual.integer(16, 97) + 'kw',
    hasTrailerHitch: () => casual.boolean,
  }),
  Query: () =>({
    people: () => new MockList([10, 12]),
    cars: () => new MockList([10, 12]),
    brands: () => brands,
  }),
};